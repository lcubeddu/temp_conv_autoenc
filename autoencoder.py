import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import torch

from layers import Sequential, Linear, Convolution, AveragePooling, Reshape, MSELoss, Sigmoid, MaxPooling, ReLU
from optimizer import Adam, SGD
from dataset import FashionMNISTDataset
from minibatch import mini_batch_training, plot_losses

X_train, X_test, _, _ = FashionMNISTDataset().load()

# Reshape to (N, C, H, W)
X_train = X_train.reshape(-1, 1, 28, 28)
X_test = X_test.reshape(-1, 1, 28, 28)

# Normalize
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train.reshape(-1, 28*28)).reshape(-1, 1, 28, 28)
X_test = scaler.transform(X_test.reshape(-1, 28*28)).reshape(-1, 1, 28, 28)

# Switch to torch tensors

X_train = torch.from_numpy(X_train).float().to("cpu")
X_train.requires_grad = False
X_test = torch.from_numpy(X_test).float().to("cpu")
X_test.requires_grad = False

# We are making a autoencoder with a convolutional encoder and a linear decoder

conv1 = Convolution(1, 4, 3, padding=1)
sigmoid1 = Sigmoid()
pool1 = MaxPooling(2, 2)
conv2 = Convolution(4, 8, 3, padding=1)
sigmoid2 = Sigmoid()
pool2 = MaxPooling(2, 2)
flatten = Reshape((8*7*7,))
linear1 = Linear(8*7*7, 392)
sigmoid3 = Sigmoid()
linear2 = Linear(392, 784)
unflatten = Reshape((1, 28, 28))
mse = MSELoss()

encoder = Sequential([conv1, sigmoid1, pool1, conv2, sigmoid2, pool2, flatten])
decoder = Sequential([linear1, sigmoid3, linear2, unflatten])

class Autoencoder:
    def __init__(self, encoder, decoder, loss_obj):
        self.encoder = encoder
        self.decoder = decoder
        self.loss_obj = loss_obj
        self.Y_hat = None

    def forward(self, X):
        self.Y_hat = self.decoder.forward(self.encoder.forward(X))
        return self.Y_hat

    def backward(self, Y):
        assert self.Y_hat is not None
        dY = self.loss_obj.backward(self.Y_hat, Y)
        return self.encoder.backward(self.decoder.backward(dY))

    def parameters(self):
        return self.encoder.parameters() + self.decoder.parameters()
    
    def gradients(self):
        return self.encoder.gradients() + self.decoder.gradients()
        
    def loss(self, y_hat, y):
        return self.loss_obj.forward(y_hat, y)

autoencoder = Autoencoder(encoder, decoder, mse)

optimizers = [Adam(autoencoder.parameters(), autoencoder.gradients(), 0.001),
                SGD(autoencoder.parameters(), autoencoder.gradients(), 0.00001)]

optimizer = optimizers[0]

train_losses, test_losses = mini_batch_training(autoencoder, X_train, X_train, X_test, X_test, 256, 5, optimizer, logging=True, sample_size=0.1)
# plot_losses(train_losses, test_losses, mode="plot")

# y = autoencoder.forward(X_test[:10])
# for i in range(10):
#     plt.subplot(2, 10, i+1)
#     plt.imshow(X_test[i, 0, :, :])
#     plt.subplot(2, 10, i+11)
#     plt.imshow(y[i, 0, :, :])
# plt.show()