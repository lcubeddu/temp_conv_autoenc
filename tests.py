from layers import _convolution_naive, _convolution_backward_naive, Convolution, ReLU, MaxPooling
import numpy as np

def __is_conv_naive_same_as_pytorch_conv():
    import torch
    
    n_im, n_c, h, w = 10, 2, 4, 4
    n_k, n_c, k_h, k_w = 3, 2, 3, 3
    
    p_h, p_w = 1, 1
    s_h, s_w = 2, 2
    
    X = np.random.randn(n_im, n_c, h, w)
    W = np.random.randn(n_k, n_c, k_h, k_w)
    b = np.random.randn(n_k)
    
    feature_map = _convolution_naive(X, W, b, padding=(p_h, p_w), stride=(s_h, s_w))
    
    X_t = torch.tensor(X)
    W_t = torch.tensor(W)
    b_t = torch.tensor(b)
    
    conv = torch.nn.Conv2d(n_c, n_k, (k_h, k_w), stride=(s_h, s_w), padding=(p_h, p_w))
    conv.weight.data = W_t
    conv.bias.data = b_t
    
    feature_map_t = conv(X_t)
    
    return np.allclose(feature_map, feature_map_t.detach().numpy())

assert __is_conv_naive_same_as_pytorch_conv(), "Convolution naive implementation is not equivalent to PyTorch's implementation"

def __is_conv_backward_naive_same_as_pythorch_conv_backward():
    import torch
    
    n_im, n_c, h, w = 10, 2, 4, 4
    n_k, n_c, k_h, k_w = 3, 2, 3, 3
    
    p_h, p_w = 1, 1
    s_h, s_w = 2, 2
    
    X = np.random.randn(n_im, n_c, h, w)
    W = np.random.randn(n_k, n_c, k_h, k_w)
    b = np.random.randn(n_k)
    
    X_t = torch.tensor(X, requires_grad=True, dtype=torch.float64)
    W_t = torch.tensor(W, requires_grad=True, dtype=torch.float64)
    b_t = torch.tensor(b, dtype=torch.float64, requires_grad=True)
    
    conv = torch.nn.Conv2d(n_c, n_k, (k_h, k_w), stride=(s_h, s_w), padding=(p_h, p_w))
    conv.weight.data = W_t
    conv.bias.data = b_t

    feature_map_t = conv(X_t)
    
    dout_t = torch.randn_like(feature_map_t)
    feature_map_t.backward(dout_t)
    
    dX_t = X_t.grad
    dW_t = conv.weight.grad
    db_t = conv.bias.grad
    
    dout = dout_t.detach().numpy()
    dX, dW, db = _convolution_backward_naive(dout, X, W, padding=(p_h, p_w), stride=(s_h, s_w))

    # db has to be checked manually because of the difference in implementation.
    # pytorch has a tied bias for each channel, but we have a bias for each output pixel.
    if __debug__:
        print("Torch db mean :", np.mean(db_t.detach().numpy()))
        print("Naive db mean :", np.mean(db))
    
    return np.allclose(dX, dX_t.detach().numpy()) and np.allclose(dW, dW_t.detach().numpy())

assert __is_conv_backward_naive_same_as_pythorch_conv_backward(), "Convolution backward naive implementation is not equivalent to PyTorch's implementation"


def test_convolution():
    # Autoencoder with convolutional layers test

    # Import MNIST dataset

    from sklearn.datasets import fetch_openml
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import StandardScaler
    mnist = fetch_openml('mnist_784', version=1, parser='auto')
    X = mnist.data

    # Split into train and test sets
    X_train, X_test = train_test_split(X, test_size=0.2)

    # Normalise data
    normalizer = StandardScaler()
    X_train = normalizer.fit_transform(X_train)
    X_test = normalizer.transform(X_test)

    # Reshape into 2d
    X_train = np.reshape(X_train, (-1, 28, 28))
    X_test = np.reshape(X_test, (-1, 28, 28))

    # Add channel dimension
    X_train = np.expand_dims(X_train, axis=1)
    X_test = np.expand_dims(X_test, axis=1)

    conv1 = Convolution(input_channels=1, output_channels=16, kernel_size=3, padding=1, stride=1)
    relu1 = ReLU()
    pool1 = MaxPooling(kernel_size=2, stride=2)
    conv2 = Convolution(input_channels=16, output_channels=32, kernel_size=3, padding=1, stride=1)
    relu2 = ReLU()
    pool2 = MaxPooling(kernel_size=2, stride=2)

    def f_pass_1(X):
        X = conv1.forward(X)
        X = relu1.forward(X)
        X = pool1.forward(X)
        X = conv2.forward(X)
        X = relu2.forward(X)
        X = pool2.forward(X)
        return X

    # Is pytorch output the same ?

    import torch
    import torch.nn as nn
    import torch.nn.functional as F

    class Net(nn.Module):
        def __init__(self):
            super().__init__()
            
            self.conv1 = nn.Conv2d(1, 16, 3, padding=1)
            self.conv2 = nn.Conv2d(16, 32, 3, padding=1)

            self.conv1.weight.data = torch.from_numpy(conv1.W)
            self.conv2.weight.data = torch.from_numpy(conv2.W)
            self.conv1.bias.data = torch.from_numpy(conv1.b)
            self.conv2.bias.data = torch.from_numpy(conv2.b)
            
        def forward(self, x):
            x = F.relu(self.conv1(x))
            x = F.max_pool2d(x, kernel_size=2, stride=2)
            x = F.relu(self.conv2(x))
            x = F.max_pool2d(x, kernel_size=2, stride=2)
            return x
        
    n2 = Net()
    x1 = f_pass_1(X_train[0:1])
    t_X = torch.from_numpy(X_train[0:1].astype(np.float64))
    t_X.requires_grad = True
    x2 = n2(t_X)
    assert np.allclose(x1, x2.detach().numpy()), "Pytorch forward output is not the same as numpy output"

    dZ = np.random.randn(*x1.shape)

    def b_pass_1(dZ):
        dZ = pool2.backward(dZ)
        dZ = relu2.backward(dZ)
        dZ = conv2.backward(dZ)
        dZ = pool1.backward(dZ)
        dZ = relu1.backward(dZ)
        dZ = conv1.backward(dZ)
        return dZ

    dX = b_pass_1(dZ)

    # Is pytorch output the same ?

    dZ2 = torch.from_numpy(dZ.astype(np.float64))
    dZ2.requires_grad = True
    x2.backward(dZ2)
    dX2 = t_X.grad.numpy()
    assert np.allclose(dX, dX2), "Pytorch backward output is not the same as numpy output"
    
test_convolution()