import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

from layers import Sequential, Linear, Convolution, AveragePooling, Reshape, MSELoss, Sigmoid, MaxPooling
from local_dataset import FashionMNISTDataset
from minibatch import mini_batch_training, plot_losses

X_train, X_test, _, _ = FashionMNISTDataset().load()

# Reshape to (N, C, H, W)
X_train = X_train.reshape(-1, 1, 28, 28)
X_test = X_test.reshape(-1, 1, 28, 28)

# Normalize
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train.reshape(-1, 28*28)).reshape(-1, 1, 28, 28)
X_test = scaler.transform(X_test.reshape(-1, 28*28)).reshape(-1, 1, 28, 28)

# We are making a autoencoder with a convolutional encoder and a linear decoder
# (1, 28, 28) -> (2, 14, 14) -> (4, 7, 7)

conv1 = Convolution(1, 2, 3, padding=1)
sigmoid1 = Sigmoid()
pool1 = AveragePooling(2, 2)
conv2 = Convolution(2, 4, 3, padding=1)
sigmoid2 = Sigmoid()
pool2 = AveragePooling(2, 2)
flatten = Reshape((4*7*7,))
linear1 = Linear(196, 392)
sigmoid3 = Sigmoid()
linear2 = Linear(392, 784)
unflatten = Reshape((1, 28, 28))
mse = MSELoss()

encoder = Sequential([conv1, sigmoid1, pool1, conv2, sigmoid2, pool2, flatten])
decoder = Sequential([linear1, sigmoid3, linear2, unflatten])

class Autoencoder:
    def __init__(self, encoder, decoder, loss_obj):
        self.encoder = encoder
        self.decoder = decoder
        self.loss_obj = loss_obj
        self.Y_hat = None

    def forward(self, X):
        self.Y_hat = self.decoder.forward(self.encoder.forward(X))
        return self.Y_hat

    def backward(self, Y):
        assert self.Y_hat is not None
        dY = self.loss_obj.backward(self.Y_hat, Y)
        return self.encoder.backward(self.decoder.backward(dY))

    def optimize(self, learning_rate):
        self.encoder.optimize(learning_rate)
        self.decoder.optimize(learning_rate)
        
    def loss(self, y_hat, y):
        return self.loss_obj.forward(y_hat, y)

autoencoder = Autoencoder(encoder, decoder, mse)

# Subset of the data
X_train = X_train[:10000, :]
X_test = X_test[:1000, :]

train_losses, test_losses = mini_batch_training(autoencoder, X_train, X_train, X_test, X_test, 100, 15, 0.0001, logging=True)
plot_losses(train_losses, test_losses, mode="plot")
y = autoencoder.forward(X_test[:10])

for i in range(10):
    plt.subplot(2, 10, i+1)
    plt.imshow(X_test[i, 0, :, :])
    plt.subplot(2, 10, i+11)
    plt.imshow(y[i, 0, :, :])
plt.show()