import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import numba

def plot_or_save(path, plot=True):
    if plot:
        plt.savefig(path)
        plt.close()
    else:
        plt.savefig(path)

def test_autoencoder(autoenc, X_train, normalizer, n_images=10, path_prefix="", plot=True):
    # Visualize Reconstructed Inputs
    for i in range(n_images):
        n = np.random.randint(0, X_train.shape[0])
        X_reconstructed = autoenc.forward(X_train[n:n+1])

        X_reconstructed = normalizer.inverse_transform(X_reconstructed)
        image = X_reconstructed[0]
        image = np.reshape(image, (28, 28))
        image_original = np.reshape(X_train[n], (28, 28))
        
        plt.imshow(image_original, cmap='gray')
        plt.title(f"Original {i}")
        plt.gcf().set_size_inches(10, 10)
        # plt.savefig(f"images/{path_prefix}_original_{i}.png")
        plot_or_save(f"images/{path_prefix}_original_{i}.png", plot=plot)
        plt.close()
        plt.imshow(image, cmap='gray')
        plt.title(f"Reconstructed {i}")
        plt.gcf().set_size_inches(10, 10)
        #plt.savefig(f"images/{path_prefix}_reconstructed_{i}.png")
        plot_or_save(f"images/{path_prefix}_reconstructed_{i}.png", plot=plot)
        plt.close()
        
@numba.njit
def correlate_2d_valid(a, b):
    a_h, a_w = a.shape
    b_h, b_w = b.shape
    
    o_h = a_h - b_h + 1
    o_w = a_w - b_w + 1
    
    out = np.zeros((o_h, o_w))
    for i in range(o_h):
        for j in range(o_w):
            out[i, j] = np.sum(a[i:i+b_h, j:j+b_w] * b)
            
    return out

@numba.njit
def correlate_2d_full(a, b):
    a_h, a_w = a.shape
    b_h, b_w = b.shape
    o_h = a_h + b_h - 1
    o_w = a_w + b_w - 1
    out = np.zeros((o_h, o_w))
    for i in range(o_h):
        for j in range(o_w):
            a_h_start = max(0, i - b_h + 1)
            a_h_end = min(a_h, i + 1)
            b_h_start = max(0, b_h - i - 1)
            b_h_end = min(b_h, a_h + b_h - i - 1)
            a_w_start = max(0, j - b_w + 1)
            a_w_end = min(a_w, j + 1)
            b_w_start = max(0, b_w - j - 1)
            b_w_end = min(b_w, a_w + b_w - j - 1)
            out[i, j] = np.sum(a[a_h_start:a_h_end, a_w_start:a_w_end] * b[b_h_start:b_h_end, b_w_start:b_w_end])
    return out
