import torch
from optimizer import Optimizer
from sklearn.metrics import accuracy_score

def batched_forward(nnet, X, batch_size):
    y_shape = nnet.forward(X[:1, :]).shape[1:]
    Y_hat = torch.zeros((X.shape[0],) + y_shape)
    for i in range(0, X.shape[0], batch_size):
        Y_hat[i:i+batch_size, :] = nnet.forward(X[i:i+batch_size, :])
    return Y_hat

def mini_batch_training(nnet, X_train, y_train, X_test, y_test, batch_size, num_epochs, optimizer: Optimizer, logging=True, sample_size=0.1):
    train_losses = []
    test_losses = []
    
    for epoch in range(num_epochs):
        # Shuffle the data
        indices = torch.randperm(X_train.shape[0])

        # Train
        batch_train_losses = []
        for i in range(0, X_train.shape[0], batch_size):
            print(f"Epoch percentage: {i/X_train.shape[0]*100:.2f}%", end="\r")
            X_batch = X_train[indices[i:i+batch_size], :]
            y_batch = y_train[indices[i:i+batch_size], :]

            Y_hat = nnet.forward(X_batch)
            nnet.backward(y_batch)
            optimizer.step()

        # Compute loss on a sample of the training data
        indices = torch.randperm(X_train.shape[0])
        X_samp = X_train[indices[:int(X_train.shape[0]*sample_size)], :]
        y_samp = y_train[indices[:int(X_train.shape[0]*sample_size)], :]
        Y_hat = batched_forward(nnet, X_samp, 256)
        train_loss = nnet.loss(Y_hat, y_samp)
        train_losses.append(train_loss)

        # Test on a sample of the test data
        indices = torch.randperm(X_test.shape[0])
        X_samp = X_test[indices[:int(X_test.shape[0]*sample_size)], :]
        y_samp = y_test[indices[:int(X_test.shape[0]*sample_size)], :]

        Y_hat = batched_forward(nnet, X_samp, 256)
        test_loss = nnet.loss(Y_hat, y_samp)
        test_losses.append(test_loss)

        if logging: 
            print(f"Epoch {epoch+1}/{num_epochs} | Train Loss: {train_loss} | Test Loss: {test_loss}")

    return train_losses, test_losses

def plot_losses(train_losses, test_losses, path_prefix="", mode="plot"):
    import matplotlib.pyplot as plt
    assert mode in ["plot", "save"]
    plt.plot(train_losses, label="Train Loss")
    plt.plot(test_losses, label="Test Loss")
    plt.legend()
    plt.title("Losses")
    plt.gcf().set_size_inches(10, 10)
    if mode == "plot":
        plt.show()
    elif mode == "save":
        plt.savefig(f"images/{path_prefix}losses.png")
    plt.close()


