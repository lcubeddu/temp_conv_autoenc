
# %%
import numpy as np
import numpy.typing as npt
import numpy.lib.stride_tricks as npst
import typing as tp
from numba import njit

class NNLayer:
    def __init__(self):
        return

    def forward(self, X):
        return

    def backward(self, dY):
        return

    def optimize(self, learning_rate: float):
        return

    def parameter_count(self):
        return

weight_initialisation_t = tp.Union[tp.Literal["normal"], tp.Literal["uniform"], tp.Literal["xavier"], tp.Literal["he"]]

class Linear(NNLayer):
    """
    Linear layer
    Dimensions:
        X: (batch_size, input_dim)
        W: (input_dim, output_dim)
        b: (output_dim)
    """
    def __init__(self, input_dim: int, output_dim: int, 
                 weight_initialisation: weight_initialisation_t = "xavier"):
        super(Linear, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.initialise_weights(weight_initialisation)
        
        self.X = None

    def __repr__(self):
        return f"Linear(input_dim={self.input_dim}, output_dim={self.output_dim})"
    
    def initialise_weights(self, method: weight_initialisation_t = "xavier"):
        """
        Initialise the weights
        Parameters:
            method: "normal", "uniform", "xavier" or "he"
        """
        if method == "normal":
            self.W = np.random.randn(self.input_dim, self.output_dim)
            self.b = np.zeros(self.output_dim)
        elif method == "uniform":
            self.W = np.random.rand(self.input_dim, self.output_dim)
            self.b = np.zeros(self.output_dim)
        elif method == "xavier":
            self.W = np.random.randn(self.input_dim, self.output_dim) / np.sqrt(self.input_dim)
            self.b = np.zeros(self.output_dim)
        elif method == "he":
            self.W = np.random.randn(self.input_dim, self.output_dim) / np.sqrt(self.input_dim / 2)
            self.b = np.zeros(self.output_dim)
        else:
            raise ValueError("Invalid weight initialisation method")

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, output_dim) Output data
        """
        assert X.shape[1] == self.input_dim, f"Input dimension mismatch. Expected {self.input_dim}, got {X.shape[1]}"
        self.X = X
        return np.dot(X, self.W) + self.b

    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Backward pass
        Parameters:
            dY: (batch_size, output_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.X is not None, "Forward pass not performed"
        assert dY.shape[1] == self.output_dim, f"Output dimension mismatch. Expected {self.output_dim}, got {dY.shape[1]}"
        self.dW = np.dot(self.X.T, dY)
        self.db = np.sum(dY, axis=0)
        return np.dot(dY, self.W.T)

    def optimize(self, learning_rate: float = 0.001):
        """
        Update the parameters
        """
        self.W -= learning_rate * self.dW
        self.b -= learning_rate * self.db

    def parameter_count(self):
        """
        Return the number of parameters in the layer
        """
        return self.input_dim * self.output_dim + self.output_dim

class ReLU(NNLayer):
    def __init__(self):
        super(ReLU, self).__init__()
        self.is_positive = None

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        ReLU forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        self.is_positive = X > 0
        return X * self.is_positive

    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        ReLU backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.is_positive is not None, "Forward pass not performed"
        return dY * self.is_positive

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

class Softmax(NNLayer):
    def __init__(self):
        super(Softmax, self).__init__()
        self.Y = None

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Softmax forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        self.X = X
        
        # Make softmax stable
        max_X = np.max(X, axis=1, keepdims=True)

        self.Y = np.exp(X-max_X) / np.sum(np.exp(X-max_X), axis=1, keepdims=True)
        return self.Y

    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Softmax backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.Y is not None, "Forward pass not performed"

        dX = []

        for i in range(dY.shape[0]):
            jacobian = np.diag(self.Y[i]) - np.outer(self.Y[i], self.Y[i])
            dX.append(np.dot(dY[i], jacobian))

        return np.array(dX) 

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

class Sigmoid(NNLayer):
    def __init__(self):
        super(Sigmoid, self).__init__()
        self.Y = None

    def positive_sigmoid(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Sigmoid forward pass for positive inputs
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        return 1 / (1 + np.exp(-X))

    def negative_sigmoid(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Sigmoid forward pass for negative inputs
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        return np.exp(X) / (1 + np.exp(X))

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Sigmoid forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        positive = X > 0
        negative = ~positive

        Y = np.empty_like(X, dtype=np.float64)

        Y[positive] = self.positive_sigmoid(X[positive])
        Y[negative] = self.negative_sigmoid(X[negative])

        self.Y = Y
        return Y

    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Sigmoid backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.Y is not None, "Forward pass not performed"
        return dY * self.Y * (1 - self.Y)

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

class Tanh(NNLayer):
    def __init__(self):
        super(Tanh, self).__init__()
        self.Y = None

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Tanh forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        self.Y = np.tanh(X)
        return self.Y

    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Tanh backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.Y is not None, "Forward pass not performed"
        return dY * (1 - self.Y * self.Y)

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

def logsumexp(X: npt.NDArray[np.float64], keepdims: bool=False) -> npt.NDArray[np.float64]:
    """
    Compute logsumexp
    Parameters:
        X: (batch_size, input_dim) Input data
    Returns:
        logsumexp: (batch_size, 1) logsumexp if keepdims is True, else (batch_size) logsumexp
    """
    max_X = np.max(X, axis=1, keepdims=True)
    return np.log(np.sum(np.exp(X - max_X), axis=1, keepdims=keepdims)) + max_X

def logsoftmax(X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
    """
    Compute logsoftmax
    Parameters:
        X: (batch_size, input_dim) Input data
    Returns:
        logsoftmax: (batch_size, input_dim) logsoftmax
    """
    return X - logsumexp(X, keepdims=True)

class CrossEntropyLoss(NNLayer):
    def __init__(self):
        super(CrossEntropyLoss, self).__init__()

    def forward(self, Y_hat: npt.NDArray[np.float64], Y: npt.NDArray[np.float64]) -> np.float64:
        """
        Cross entropy loss forward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            loss: (np.float64) Loss
        """
        individual_loss = np.sum(- Y * np.log(Y_hat), axis=1)
        return np.mean(individual_loss)

    def backward(self, Y_hat: npt.NDArray[np.float64], Y: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Cross entropy loss backward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            dY_hat: (batch_size, input_dim) Gradients of the next layer
        """
        return - Y / Y_hat

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

class SoftmaxCrossEntropyLoss(NNLayer):
    def __init__(self):
        super(SoftmaxCrossEntropyLoss, self).__init__()
        self.softmax = Softmax()
        self.cross_entropy_loss = CrossEntropyLoss()
        self.Y = None

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Softmax cross entropy loss forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y_hat: (batch_size, input_dim) Predicted values
        """
        self.Y_hat = self.softmax.forward(X)
        return self.Y_hat

    def loss(self, Y_hat: npt.NDArray[np.float64], Y: npt.NDArray[np.float64]) -> np.float64:
        """
        Softmax cross entropy loss forward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            loss: (np.float64) Loss
        """
        return np.mean(logsumexp(Y_hat) - np.sum(Y * Y_hat, axis=1))

    def backward(self, Y: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Softmax cross entropy loss backward pass
        Parameters:
            Y: (batch_size, input_dim) Actual values
        Returns:
            dX: (batch_size, input_dim) Gradients of the current layer
        """
        return self.Y_hat - Y

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

class MSELoss(NNLayer):
    def __init__(self):
        super(MSELoss, self).__init__()

    def forward(self, Y_hat: npt.NDArray[np.float64], Y: npt.NDArray[np.float64]) -> np.float64:
        """
        MSE loss forward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            loss: (np.float64) Loss
        """
        return np.mean(np.sum((Y_hat - Y) ** 2, axis=1))

    def backward(self, Y_hat: npt.NDArray[np.float64], Y: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        MSE loss backward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            dY_hat: (batch_size, input_dim) Gradients of the next layer
        """
        return 2 * (Y_hat - Y)

    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

class Sequential(NNLayer):
    def __init__(self, layers: tp.List[NNLayer]):
        super(Sequential, self).__init__()
        self.layers = layers

    def add(self, layer: NNLayer):
        """
        Add a layer to the sequential model
        """
        self.layers.append(layer)

    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Sequential forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, output_dim) Output data
        """
        for layer in self.layers:
            X = layer.forward(X)
            if(np.max(np.abs(X)) > 1000):
                print(np.max(X))
                print("Value too high in layer", layer)    
            if(np.any(np.isnan(X))):
                print(layer)
                print(X)
                raise ValueError("NaN in forward pass")
        return X

    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Sequential backward pass
        Parameters:
            dY: (batch_size, output_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        for layer in reversed(self.layers):
            dY = layer.backward(dY)
            if(np.max(np.abs(dY)) > 1000):
                print(np.max(dY))
                print("Value too high in layer", layer)    
        return dY

    def optimize(self, learning_rate: float = 0.0001):
        """
        Update the parameters
        """
        for layer in self.layers:
            layer.optimize(learning_rate=learning_rate)

    def parameter_count(self):
        """
        Return the number of parameters in the layer
        """
        return sum([layer.parameter_count() for layer in self.layers])
    
# TODO THIS IS A TEMPORARY IMPLEMENTATION OF CONVOLUTIONAL LAYERS

# Image shape: (n_im, n_c, h, w)
#     n_im: Number of images
#     n_c: Number of channels
#     h: Height
#     w: Width

# Kernel shape: (n_k, n_c, h_k, w_k)
#     n_k: Number of kernels
#     n_c: Number of channels
#     h_k: Kernel height
#     w_k: Kernel width

@njit
def __correlate_2d_valid(a, b):
    a_h, a_w = a.shape
    b_h, b_w = b.shape
    
    o_h = a_h - b_h + 1
    o_w = a_w - b_w + 1
    
    out = np.zeros((o_h, o_w))
    for i in range(o_h):
        for j in range(o_w):
            out[i, j] = np.sum(a[i:i+b_h, j:j+b_w] * b)
            
    return out

@njit
def _correlate_2d_full(a, b):
    a_h, a_w = a.shape
    b_h, b_w = b.shape
    o_h = a_h + b_h - 1
    o_w = a_w + b_w - 1
    out = np.zeros((o_h, o_w))
    for i in range(o_h):
        for j in range(o_w):
            a_h_start = max(0, i - b_h + 1)
            a_h_end = min(a_h, i + 1)
            b_h_start = max(0, b_h - i - 1)
            b_h_end = min(b_h, a_h + b_h - i - 1)
            a_w_start = max(0, j - b_w + 1)
            a_w_end = min(a_w, j + 1)
            b_w_start = max(0, b_w - j - 1)
            b_w_end = min(b_w, a_w + b_w - j - 1)
            out[i, j] = np.sum(a[a_h_start:a_h_end, a_w_start:a_w_end] * b[b_h_start:b_h_end, b_w_start:b_w_end])
    return out

@njit
def _convolution_naive(X, W, b, padding=(0, 0), stride=(1, 1)):
    """
        X : (n_im, n_c, h, w) input
        W : (n_k, n_c, k_h, k_w) kernel weights
        b : (n_k, h_out, w_out) bias
    returns
        Z : (n_im, n_k, h_out, w_out) feature map
    """
    n_im, n_c, h, w = X.shape
    n_k, n_c, k_h, k_w = W.shape
    
    p_h, p_w = padding
    s_h, s_w = stride
    
    h_out = (h + 2 * p_h - k_h) // s_h + 1
    w_out = (w + 2 * p_w - k_w) // s_w + 1

    assert h_out % s_h == 0, "Invalid stride"
    assert w_out % s_w == 0, "Invalid stride"
    assert p_h < k_h and p_w < k_w, "Invalid padding, padding must be less than kernel size"
    
    Z = np.zeros((n_im, n_k, h_out, w_out))
    for i in range(n_im):
        for j in range(n_k):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = max(0, k * s_h - p_h)
                    x_h_end = min(h, k * s_h + k_h - p_h)
                    x_w_start = max(0, l * s_w - p_w)
                    x_w_end = min(w, l * s_w + k_w - p_w)
                    
                    X_slice = X[i, :, x_h_start:x_h_end, x_w_start:x_w_end]

                    w_h_start = max(0, p_h - k * s_h)
                    w_h_end = min(k_h, h + p_h - k * s_h)
                    w_w_start = max(0, p_w - l * s_w)
                    w_w_end = min(k_w, w + p_w - l * s_w)
                    
                    W_slice = W[j, :, w_h_start:w_h_end, w_w_start:w_w_end]

                    Z[i, j, k, l] = np.sum(X_slice * W_slice) + b[j]
                    
    return Z

def _convolution_sliding_window_view(X, W, padding=(0, 0), stride=(1, 1)):
    npst.sliding_window_view()
    

@njit
def _convolution_backward_naive(dZ, X, W, padding=(0, 0), stride=(1, 1)):
    """
        dZ : (n_im, n_k, h_out, w_out) gradients of feature map
        X : (n_im, n_c, h, w) input
        W : (n_k, n_c, k_h, k_w) kernel weights
    returns
        dX : (n_im, n_c, h, w) gradients of input
        dW : (n_k, n_c, k_h, k_w) gradients of kernel weights
        db : (n_k, h_out, w_out) gradients of bias
    """
    n_im, n_k, h_out, w_out = dZ.shape
    n_im, n_c, h, w = X.shape
    n_k, n_c, k_h, k_w = W.shape
    
    p_h, p_w = padding
    s_h, s_w = stride
    
    dX = np.zeros((n_im, n_c, h, w))
    dW = np.zeros((n_k, n_c, k_h, k_w))
    db = np.zeros(n_k)
    
    for i in range(n_im):
        for j in range(n_k):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = max(0, k * s_h - p_h)
                    x_h_end = min(h, k * s_h + k_h - p_h)
                    x_w_start = max(0, l * s_w - p_w)
                    x_w_end = min(w, l * s_w + k_w - p_w)

                    X_slice = X[i, :, x_h_start:x_h_end, x_w_start:x_w_end]
                    dX_slice = dX[i, :, x_h_start:x_h_end, x_w_start:x_w_end]
                    
                    w_h_start = max(0, p_h - k * s_h)
                    w_h_end = min(k_h, h + p_h - k * s_h)
                    w_w_start = max(0, p_w - l * s_w)
                    w_w_end = min(k_w, w + p_w - l * s_w)

                    W_slice = W[j, :, w_h_start:w_h_end, w_w_start:w_w_end]
                    dW_slice = dW[j, :, w_h_start:w_h_end, w_w_start:w_w_end]
                    
                    dW_slice += X_slice * dZ[i, j, k, l]
                    dX_slice += W_slice * dZ[i, j, k, l]
                    
    for i in range(n_k):
        db[i] = np.sum(dZ[:, i, :, :])
        
    return dX, dW, db

@njit
def _average_pooling_naive(X, kernel_size=(2, 2), stride=(2, 2)):
    """
        X : (n_im, n_c, h, w) input
    returns
        Z : (n_im, n_c, h_out, w_out) feature map
    """
    n_im, n_c, h, w = X.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    h_out = h // s_h
    w_out = w // s_w
    
    assert h_out * s_h == h, "Invalid stride"
    assert w_out * s_w == w, "Invalid stride"
    
    Z = np.zeros((n_im, n_c, h_out, w_out))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    Z[i, j, k, l] = np.mean(X[i, j, x_h_start:x_h_end, x_w_start:x_w_end])
    return Z

@njit
def _max_pooling_naive(X, kernel_size=(2, 2), stride=(2, 2)):
    """
        X : (n_im, n_c, h, w) input
    returns
        Z : (n_im, n_c, h_out, w_out) feature map
    """
    n_im, n_c, h, w = X.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    h_out = h // s_h
    w_out = w // s_w
    
    assert h_out * s_h == h, "Invalid stride"
    assert w_out * s_w == w, "Invalid stride"
    
    Z = np.zeros((n_im, n_c, h_out, w_out))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    Z[i, j, k, l] = np.max(X[i, j, x_h_start:x_h_end, x_w_start:x_w_end])
    return Z

@njit
def _average_pooling_backward_naive(dZ, X, kernel_size=(2, 2), stride=(2, 2)):
    """
        dZ : (n_im, n_c, h_out, w_out) gradients of feature map
        X : (n_im, n_c, h, w) input
    returns
        dX : (n_im, n_c, h, w) gradients of input
    """
    n_im, n_c, h, w = X.shape
    n_im, n_c, h_out, w_out = dZ.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    dX = np.zeros((n_im, n_c, h, w))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    dX[i, j, x_h_start:x_h_end, x_w_start:x_w_end] += dZ[i, j, k, l] / (k_h * k_w)
    return dX

@njit
def _max_pooling_backward_naive(dZ, X, cached_Z, kernel_size=(2, 2), stride=(2, 2)):
    """
        dZ : (n_im, n_c, h_out, w_out) gradients of feature map
        X : (n_im, n_c, h, w) input
    returns
        dX : (n_im, n_c, h, w) gradients of input
    """
    n_im, n_c, h, w = X.shape
    n_im, n_c, h_out, w_out = dZ.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    dX = np.zeros((n_im, n_c, h, w))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    X_slice = X[i, j, x_h_start:x_h_end, x_w_start:x_w_end]
                    dX_slice = dX[i, j, x_h_start:x_h_end, x_w_start:x_w_end]
                    
                    Z_slice = cached_Z[i, j, k, l]
                    
                    dX_slice += (X_slice == Z_slice) * dZ[i, j, k, l]
    return dX

class AveragePooling(NNLayer):
    def __init__(self, kernel_size: int, stride: int):
        super(AveragePooling, self).__init__()
        self.kernel_size = (kernel_size, kernel_size)
        self.stride = (stride, stride)
        self.cached_X = None
    
    def __repr__(self):
        return f"MeanPooling(kernel_size={self.kernel_size}, stride={self.stride})"
    
    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_channels, h, w) Input data
        Returns:
            Y: (batch_size, output_channels, h_out, w_out) Output data
        """
        self.cached_X = X
        return _average_pooling_naive(X, kernel_size=self.kernel_size, stride=self.stride)
    
    def backward(self, dZ: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Backward pass
        Parameters:
            dZ: (batch_size, output_channels, h_out, w_out) Gradients of the next layer
        Returns:
            dX: (batch_size, input_channels, h, w) Gradients of this layer
        """
        assert self.cached_X is not None, "Forward pass not performed"
        return _average_pooling_backward_naive(dZ, self.cached_X, kernel_size=self.kernel_size, stride=self.stride)
    
    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return
    
    def parameter_count(self):
        return 0
    
class MaxPooling(NNLayer):
    def __init__(self, kernel_size: int, stride: int):
        super(MaxPooling, self).__init__()
        self.kernel_size = (kernel_size, kernel_size)
        self.stride = (stride, stride)
        self.cached_X = None
        self.cached_Z = None
    
    def __repr__(self):
        return f"MaxPooling(kernel_size={self.kernel_size}, stride={self.stride})"
    
    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_channels, h, w) Input data
        Returns:
            Y: (batch_size, output_channels, h_out, w_out) Output data
        """
        self.cached_X = X
        self.cached_Z = _max_pooling_naive(X, kernel_size=self.kernel_size, stride=self.stride)
        return self.cached_Z
    
    def backward(self, dZ: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Backward pass
        Parameters:
            dZ: (batch_size, output_channels, h_out, w_out) Gradients of the next layer
        Returns:
            dX: (batch_size, input_channels, h, w) Gradients of this layer
        """
        assert self.cached_X is not None, "Forward pass not performed"
        assert self.cached_Z is not None, "Forward pass not performed"
        return _max_pooling_backward_naive(dZ, self.cached_X, self.cached_Z, kernel_size=self.kernel_size, stride=self.stride)
    
    def optimize(self, learning_rate):
        """
        No parameters to update
        """
        return
    
    def parameter_count(self):
        return 0

class Convolution(NNLayer):
    def __init__(self, input_channels: int, output_channels: int, kernel_size: int,
                 padding: int=0, stride: int=1, weight_initialisation: weight_initialisation_t = "xavier"):
        super(Convolution, self).__init__()

        self.input_channels = input_channels
        self.output_channels = output_channels
        self.kernel_size = kernel_size
        self.padding = (padding, padding)
        self.stride = (stride, stride)

        self.initialise_weights(weight_initialisation)
        self.X = None
        self.dW = None
        self.db = None
        
    def __repr__(self):
        return f"Convolution(input_channels={self.input_channels}, output_channels={self.output_channels}, kernel_size={self.kernel_size}, padding={self.padding}, stride={self.stride})"
    
    def initialise_weights(self, method: weight_initialisation_t = "xavier"):
        """
        Initialise the weights
        Parameters:
            method: "normal", "uniform", "xavier" or "he"
        """
        if method == "normal":
            self.W = np.random.randn(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size)
            self.b = np.zeros(self.output_channels)
        elif method == "uniform":
            self.W = np.random.rand(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size)
            self.b = np.zeros(self.output_channels)
        elif method == "xavier":
            self.W = np.random.randn(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size) / np.sqrt(self.input_channels)
            self.b = np.zeros(self.output_channels)
        elif method == "he":
            self.W = np.random.randn(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size) / np.sqrt(self.input_channels / 2)
            self.b = np.zeros(self.output_channels)
        else:
            raise ValueError("Invalid weight initialisation method")
        
    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_channels, h, w) Input data
        Returns:
            Y: (batch_size, output_channels, h_out, w_out) Output data
        """
        assert X.shape[1] == self.input_channels, f"Input dimension mismatch. Expected {self.input_channels}, got {X.shape[1]}"
        self.X = X
        return _convolution_naive(X, self.W, self.b, padding=self.padding, stride=self.stride)

    def backward(self, dZ: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """
        Perform a Backward pass
        Parameters:
            dZ: (batch_size, output_channels, h_out, w_out) Gradients of the next layer
        Returns:
            dX: (batch_size, input_channels, h, w) Gradients of this layer
        """
        assert self.X is not None, "Forward pass not performed"
        assert dZ.shape[1] == self.output_channels, f"Output dimension mismatch. Expected {self.output_channels}, got {dZ.shape[1]}"
        dX, dW, db = _convolution_backward_naive(dZ, self.X, self.W, padding=self.padding, stride=self.stride)
        self.dW = dW
        self.db = db
        return dX
    
    def optimize(self, learning_rate: float = 0.01):
        """
        Update the parameters
        """
        self.W -= learning_rate * self.dW
        self.b -= learning_rate * self.db
        
    def output_shape(self, input_shape: tp.Tuple[int, int, int, int]) -> tp.Tuple[int, int, int, int]:
        """
        Return the output shape given an input shape
        """
        # TODO verify the correctness of this
        n_im, n_c, h, w = input_shape
        h_out = (h + 2 * self.padding[0] - self.kernel_size) // self.stride[0] + 1
        w_out = (w + 2 * self.padding[1] - self.kernel_size) // self.stride[1] + 1
        return (n_im, self.output_channels, h_out, w_out)
    
    def parameter_count(self):
        return self.output_channels * self.input_channels * self.kernel_size * self.kernel_size + self.output_channels

class Reshape(NNLayer):
    def __init__(self, new_shape: tp.Iterable):
        self.new_shape = new_shape
        self.old_shape = None

    def __repr__(self):
        if self.old_shape:
            return f"Reshape(old_shape={self.old_shape}, new_shape={self.new_shape})"
        else:
            return f"Reshape(new_shape={self.new_shape})"
    
    def forward(self, X: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        self.old_shape = X.shape[1:]
        batch_size = X.shape[0]
        return np.reshape(X, (batch_size,) + self.new_shape)
    
    def backward(self, dY: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        assert self.old_shape is not None, "Forward pass not performed"
        batch_size = dY.shape[0]
        return np.reshape(dY, (batch_size,) + self.old_shape)
    
    def optimize(self, learning_rate: float):
        """
        No parameters to update
        """
        return
    
    def reverse(self):
        inv = Reshape(self.old_shape)
        inv.old_shape = self.new_shape
        return inv

    def parameter_count(self):
        """
        No parameters
        """
        return 0