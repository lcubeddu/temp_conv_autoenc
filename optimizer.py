from typing import List
import torch

class Optimizer:
    def __init__(self,
                 parameters: List[torch.Tensor],
                 gradients: List[torch.Tensor]):
        self.parameters = parameters
        self.gradients = gradients

    def step(self):
        raise NotImplementedError
    
class SGD(Optimizer):
    def __init__(self,
                 parameters: List[torch.Tensor],
                 gradients: List[torch.Tensor],
                 lr: float):
        super().__init__(parameters, gradients)
        self.lr = lr

    def step(self):
        for param, grad in zip(self.parameters, self.gradients):
            param[:] -= self.lr * grad
    
class Adam(Optimizer):
    # https://www.deeplearningbook.org/contents/optimization.html
    # page 306
    def __init__(self,
                 parameters: List[torch.Tensor],
                 gradients: List[torch.Tensor],
                 lr: float,
                 beta1: float = 0.9,
                 beta2: float = 0.999,
                 eps: float = 1e-8):
        super().__init__(parameters, gradients)
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.eps = eps
        self.m = [torch.zeros_like(param) for param in self.parameters]
        self.v = [torch.zeros_like(param) for param in self.parameters]
        self.t = 0

    def step(self):
        self.t += 1
        for param, grad, m, v in zip(self.parameters, self.gradients, self.m, self.v):
            m[:] = self.beta1 * m + (1 - self.beta1) * grad
            v[:] = self.beta2 * v + (1 - self.beta2) * grad**2
            m_hat = m / (1 - self.beta1**self.t)
            v_hat = v / (1 - self.beta2**self.t)
            param[:] -= self.lr * m_hat / (v_hat.sqrt() + self.eps)