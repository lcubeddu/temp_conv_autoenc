from typing import List
from autoencoder import Autoencoder, test_autoencoder
from minibatch import plot_losses
from local_dataset import FashionMNISTDataset
from sklearn.preprocessing import StandardScaler
from models import Sequential, Linear
import numpy as np
from umap import UMAP

def main():
    # Load dataset
    dataset = FashionMNISTDataset()
    target_class_names: List[str] = ["T-shirt/top", "Trouser", "Pullover", "Dress", "Coat", "Sandal",
                                     "Shirt", "Sneaker", "Bag", "Ankle boot"]
    X_train, _, y_train, _ = dataset.load()
    X_train = np.reshape(X_train, (-1, 784))

    # Normalize
    normalizer = StandardScaler()
    X_train = normalizer.fit_transform(X_train)

    # Load the autoencoder
    import pickle
    with open("autoenc_results_2.pkl", "rb") as f:
        results = pickle.load(f)
    
    autoencoder: Autoencoder = results["autoenc"] 
    normalizer = results["normalizer"]
    train_losses = results["train_losses"]
    test_losses = results["test_losses"]
    
    layers: List[Sequential] = autoencoder.model.layers
    encoder: Sequential = autoencoder.model.layers[0]
    decoder: Sequential = autoencoder.model.layers[1]

    if False:
        # Encode X_train
        X_train_encoded = encoder.forward(X_train)

        # UMAP Plot of the latent variables
        umap = UMAP(n_components=2)
        X_train_encoded_umap = umap.fit_transform(X_train_encoded)

        import matplotlib.pyplot as plt
        fig, ax = plt.subplots()
        for i in range(10):
            ax.scatter(X_train_encoded_umap[y_train == i, 0], X_train_encoded_umap[y_train == i, 1], s=0.1, label=target_class_names[i])
        ax.legend()
        ax.set_title("UMAP of the latent variables")
        plt.savefig("umap_latent_variables.png")
        plt.show()

    linear_layers = [l for l in encoder.layers if l.__class__.__name__ == "Linear"]
    print("Linear layers:", linear_layers)
    l1: Linear = linear_layers[0]
    l2: Linear = linear_layers[1]

    l1_weights = l1.W
    # pick the 64 first weights
    l1_weights = l1_weights[:, 0:64]
    print(l1_weights.shape)
    # reshape them to 28x28
    l1_weights = np.reshape(l1_weights, (64, 28, 28))
    # plot them
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(8, 8)
    for i in range(8):
        for j in range(8):
            ax[i, j].imshow(l1_weights[i*8 + j], cmap="gray")
            ax[i, j].axis("off")
    plt.savefig("l1_weights.png")
    plt.show()

    return

if __name__ == "__main__":
    main()
