import os
import numpy as np
from typing import Tuple
from dataset_download import download_affnist, download_tiny_imagenet
import torch

class FashionMNISTDataset:
    def __init__(self) -> None:
        pass

    def load(self) -> Tuple[np.array, np.array, np.array, np.array]:
        """
        Loads the split fashion mnist dataset
        Returns:
            X_train, X_test, y_train, y_test: np.array the split dataset
        """
        filepath = os.path.dirname(os.path.abspath(__file__))
        assert os.path.exists(f'{filepath}/data/fashion-mnist.npz'), "Dataset not found."
        data = np.load(f'{filepath}/data/fashion-mnist.npz')
        X_train = data["X_train"]
        X_test = data["X_test"]
        y_train = data["y_train"]
        y_test = data["y_test"]
        return X_train, X_test, y_train, y_test

class TinyImagenetDataset:
    def __init__(self) -> None:
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.path = None
        pass

    def download(self, path) -> None:
        """
        Downloads and saves the tiny imagenet dataset to specified path
        Returns:
            X_train, X_test, y_train, y_test: np.array the split dataset
        """
        X_train, X_test, y_train, y_test = download_tiny_imagenet()
        self.path = path
        if not os.path.exists(path):
            os.makedirs(path)
        np.savez(f"{path}/tiny-imagenet.npz", X_train=X_train, X_test=X_test, y_train=y_train, y_test=y_test)

    def load(self, path=None) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """
        Loads the split tiny imagenet dataset from specified path
        Returns:
            X_train, X_test, y_train, y_test: np.array the split dataset
        """
        if self.X_train is not None and self.X_test is not None and self.y_train is not None and self.y_test is not None:
            return self.X_train, self.X_test, self.y_train, self.y_test
        if path is None and self.path is None:
            raise Exception("Path not specified")
        if path is None:
            path = self.path
        assert os.path.exists(f"{path}/tiny-imagenet.npz"), "Dataset not found."
        data = np.load(f"{path}/tiny-imagenet.npz")
        self.X_train = torch.from_numpy(data["X_train"]).float()
        self.X_test = torch.from_numpy(data["X_test"]).float()
        self.y_train = torch.from_numpy(data["y_train"]).long()
        self.y_test = torch.from_numpy(data["y_test"]).long()
        return self.X_train, self.X_test, self.y_train, self.y_test
    
class AffNISTDataset:
    def __init__(self) -> None:
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.path = None
        pass

    def download(self) -> None:
        """
        Downloads and saves the affnist dataset to specified path
        Returns:
            X_train, X_test, y_train, y_test: np.array the split dataset
        """
        self.X_train, self.X_test, self.y_train, self.y_test = download_affnist()
        path = 'data'
        if not os.path.exists(path):
            os.makedirs(path)
        np.savez(f"{path}/affnist.npz", X_train=self.X_train, X_test=self.X_test, y_train=self.y_train, y_test=self.y_test)
        
    def load(self) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """
        Loads the split affnist dataset from specified path
        Returns:
            X_train, X_test, y_train, y_test: np.array the split dataset
        """
        if self.X_train is not None and self.X_test is not None and self.y_train is not None and self.y_test is not None:
            return self.X_train, self.X_test, self.y_train, self.y_test
        assert os.path.exists(f"data/affnist.npz"), "Dataset not found."
        data = np.load(f"data/affnist.npz")
        self.X_train = torch.from_numpy(data["X_train"]).float()
        self.X_test = torch.from_numpy(data["X_test"]).float()
        self.y_train = torch.from_numpy(data["y_train"]).long()
        self.y_test = torch.from_numpy(data["y_test"]).long()
        return self.X_train, self.X_test, self.y_train, self.y_test
        