from datasets import load_dataset
from typing import Dict, Tuple
import scipy
import torch
import numpy as np
import urllib.request
import os
import zipfile

def _to_numpy_pil_tiny_imagenet(pil: list):
    for i, image in enumerate(pil):
        if np.asarray(image).shape == (64, 64):
            pil[i] = np.asarray(image).reshape((64, 64, 1)).repeat(3, axis=2)
        elif np.array(image).shape != (64, 64, 3):
            print("WARNING: Image shape is not (64, 64, 3) or (64, 64, 1)")
            print(f"At index {i}, image shape is {np.array(image).shape}")
            raise Exception("Invalid image shape")
        else:
            pil[i] = np.asarray(image)
    return np.asarray(pil, dtype=np.float32)

def download_tiny_imagenet() -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
    """
    Downloads the Tiny ImageNet dataset and returns the training and testing data.

    Returns:
        Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]: A tuple containing the training and testing data.
            - X_train: Training images as torch.Tensor.
            - X_test: Testing images as torch.Tensor.
            - y_train: Training labels as torch.Tensor.
            - y_test: Testing labels as torch.Tensor.
    """
    dataset = load_dataset('Maysee/tiny-imagenet', split=['train', 'valid'])
    X_train = torch.from_numpy(_to_numpy_pil_tiny_imagenet(dataset[0]['image'])).float()
    X_test = torch.from_numpy(_to_numpy_pil_tiny_imagenet(dataset[1]['image'])).float()
    y_train = torch.from_numpy(np.array(dataset[0]['label'])).long()
    y_test = torch.from_numpy(np.array(dataset[1]['label'])).long()
    return X_train, X_test, y_train, y_test

def download_affnist() -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
    """
    Huge fucking dataset, where the author could have just provided us with a way to make
    the transforms on MNIST, but instead decided that downloading a 500M and a 100M matrix in 
    a ridiculous proprietary format would be funny.

    returns X_train, X_test, y_train, y_test
    """
    train_batches_url = 'https://www.cs.toronto.edu/~tijmen/affNIST/32x/transformed/training_batches.zip'
    test_batches_url = 'https://www.cs.toronto.edu/~tijmen/affNIST/32x/transformed/test_batches.zip'
    if not os.path.exists('data/affnist'):
        os.makedirs('data/affnist')
    if os.path.exists('data/affnist/test_batches.zip') or os.path.exists('data/affnist/training_batches.zip'):
        raise Exception("Affnist already downloaded")
    urllib.request.urlretrieve(test_batches_url, 'data/test_batches.zip')
    urllib.request.urlretrieve(train_batches_url, 'data/training_batches.zip')
    with zipfile.ZipFile('data/training_batches.zip', 'r') as zip_ref:
        zip_ref.extractall('data/affnist')
    with zipfile.ZipFile('data/test_batches.zip', 'r') as zip_ref:
        zip_ref.extractall('data/affnist')
    # We only load in the first batch
    def affNIST_mat_to_tensors(mat_file) -> Dict[str, torch.Tensor]:
        """
        Converts a .mat file from the affNIST dataset to two tensors.
        """
        data_dict = scipy.io.loadmat(mat_file)['affNISTdata']
        images = data_dict['image'][0, 0].transpose()
        labels = data_dict['label_int'][0, 0]
        return {
            'X': torch.from_numpy(images).float(),
            'y': torch.from_numpy(labels).long().flatten()
        }
    train_batch = affNIST_mat_to_tensors('data/affnist/training_batches/1.mat')
    test_batch = affNIST_mat_to_tensors('data/affnist/test_batches/1.mat')
    return train_batch['X'], test_batch['X'], train_batch['y'], test_batch['y']
    