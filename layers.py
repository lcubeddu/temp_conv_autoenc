from typing import List, Tuple, Union, Literal
import torch
from torch._tensor import Tensor

# Disable gradient calculation
torch.set_grad_enabled(False)
torch.device("cuda" if torch.cuda.is_available() else "cpu")

weight_initialisation_t = Union[Literal["normal"], Literal["uniform"], Literal["xavier"], Literal["he"]]

class Layer:
    def __init__(self):
        pass

    def __repr__(self):
        return f"{self.__class__.__name__}()"

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, output_dim) Output data
        """
        raise NotImplementedError
    
    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        Backward pass
        Parameters:
            dY: (batch_size, output_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        raise NotImplementedError
    
    def parameters(self) -> List[torch.Tensor]:
        """
        Return the parameters of the layer
        """
        return []
    
    def gradients(self) -> List[torch.Tensor]:
        """
        Return the gradients of the layer
        """
        return []
    
    def parameter_count(self):
        """
        Return the number of parameters in the layer
        """
        raise NotImplementedError

class Linear(Layer):
    """
    Linear layer
    Dimensions:
        X: (batch_size, input_dim)
        W: (input_dim, output_dim)
        b: (output_dim)
    """
    def __init__(self, input_dim: int, output_dim: int, 
                 weight_initialisation: weight_initialisation_t = "xavier"):
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim

        self.initialise_weights(weight_initialisation)

        self.X = None

    def __repr__(self):
        return f"Linear(input_dim={self.input_dim}, output_dim={self.output_dim})"
    
    def initialise_weights(self, method: weight_initialisation_t = "xavier"):
        """
        Initialise the weights
        Parameters:
            method: "normal", "uniform", "xavier" or "he"
        """
        if method == "normal":
            self.W = torch.randn(self.input_dim, self.output_dim)
            self.b = torch.zeros(self.output_dim)
        elif method == "uniform":
            self.W = torch.rand(self.input_dim, self.output_dim)
            self.b = torch.zeros(self.output_dim)
        elif method == "xavier":
            self.W = torch.randn(self.input_dim, self.output_dim) / torch.sqrt(torch.tensor(self.input_dim).float())
            self.b = torch.zeros(self.output_dim)
        elif method == "he":
            self.W = torch.randn(self.input_dim, self.output_dim) / torch.sqrt(torch.tensor(self.input_dim / 2).float())
            self.b = torch.zeros(self.output_dim)
        else:
            raise ValueError("Invalid weight initialisation method")
        self.dW = torch.zeros_like(self.W)
        self.db = torch.zeros_like(self.b)

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, output_dim) Output data
        """
        assert X.shape[1] == self.input_dim, f"Input dimension mismatch. Expected {self.input_dim}, got {X.shape[1]}"
        self.X = X
        return torch.matmul(X, self.W) + self.b

    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        Perform a Backward pass
        Parameters:
            dY: (batch_size, output_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.X is not None, "Forward pass not performed"
        assert dY.shape[1] == self.output_dim, f"Output dimension mismatch. Expected {self.output_dim}, got {dY.shape[1]}"
        self.dW[:] = torch.matmul(self.X.T, dY)
        self.db[:] = torch.sum(dY, dim=0)
        return torch.matmul(dY, self.W.T)

    def parameters(self) -> List[torch.Tensor]:
        """
        Return the parameters of the layer
        """
        return [self.W, self.b]

    def gradients(self) -> List[torch.Tensor]:
        return [self.dW, self.db]

    def parameter_count(self):
        """
        Return the number of parameters in the layer
        """
        return self.input_dim * self.output_dim + self.output_dim
    
# Test that Linear has same results as linear from old_layers.py that use np

def test_linear():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5
    output_dim = 3

    X = torch.randn(batch_size, input_dim)

    linear = Linear(input_dim, output_dim)
    W = linear.W
    b = linear.b

    Y = linear.forward(X)

    linear2 = ol.Linear(input_dim, output_dim)
    linear2.W = W.detach().numpy()
    linear2.b = b.detach().numpy()

    Y2 = linear2.forward(X.numpy())


    # Backward
    
    dY = torch.randn(batch_size, output_dim)
    
    dX = linear.backward(dY)

    dX2 = linear2.backward(dY.numpy())

    return np.allclose(Y.detach().numpy(), Y2) and np.allclose(dX.detach().numpy(), dX2)


class ReLU(Layer):
    def __init__(self):
        super().__init__()
        self.is_positive = None

    parameters = Layer.parameters
    gradients = Layer.gradients

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        ReLU forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        self.is_positive = X > 0
        return X * self.is_positive

    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        ReLU backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.is_positive is not None, "Forward pass not performed"
        return dY * self.is_positive

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0
    
# Test that ReLU has same results as relu from old_layers.py that use numpy

def test_relu():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    X = torch.randn(batch_size, input_dim)
    relu = ReLU()
    Y = relu.forward(X)
    relu2 = ol.ReLU()
    Y2 = relu2.forward(X.numpy())

    # Backward

    dY = torch.randn(batch_size, input_dim)
    dX = relu.backward(dY)
    dX2 = relu2.backward(dY.numpy())

    return np.allclose(Y.detach().numpy(), Y2) and np.allclose(dX.detach().numpy(), dX2)

class Softmax(Layer):
    def __init__(self):
        self.Y = None

    parameters = Layer.parameters
    gradients = Layer.gradients

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Softmax forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        self.X = X
        
        # Make softmax stable
        max_X, _ = torch.max(X, dim=1, keepdim=True)

        self.Y = torch.exp(X - max_X) / torch.sum(torch.exp(X - max_X), dim=1, keepdim=True)
        return self.Y

    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        Softmax backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.Y is not None, "Forward pass not performed"

        dX = torch.empty_like(dY)

        for i in range(dY.shape[0]):
            jacobian = torch.diag(self.Y[i]) - torch.outer(self.Y[i], self.Y[i])
            dX[i] = torch.matmul(dY[i], jacobian)

        return dX

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0
    
# Softmax other version to test later and maybe implement
# jacobian = np.diag(self.Y) - np.einsum('ij,ik->ijk', self.Y, self.Y)
# dX = np.einsum('ij,ijk->ik', dY, jacobian)


# Test that Softmax has same results as softmax from old_layers.py that use numpy

def test_softmax():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    X = torch.randn(batch_size, input_dim)
    softmax = Softmax()
    Y = softmax.forward(X)
    softmax2 = ol.Softmax()
    Y2 = softmax2.forward(X.numpy())

    # Backward

    dY = torch.randn(batch_size, input_dim)
    dX = softmax.backward(dY)
    dX2 = softmax2.backward(dY.numpy())

    return np.allclose(Y.detach().numpy(), Y2) and np.allclose(dX.detach().numpy(), dX2)


class Sigmoid(Layer):
    def __init__(self):
        self.Y = None

    parameters = Layer.parameters
    gradients = Layer.gradients

    def positive_sigmoid(self, X: torch.Tensor) -> torch.Tensor:
        """
        Sigmoid forward pass for positive inputs
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        return 1 / (1 + torch.exp(-X))

    def negative_sigmoid(self, X: torch.Tensor) -> torch.Tensor:
        """
        Sigmoid forward pass for negative inputs
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        return torch.exp(X) / (1 + torch.exp(X))

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Sigmoid forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        positive = X > 0
        negative = ~positive

        Y = torch.empty_like(X)

        Y[positive] = self.positive_sigmoid(X[positive])
        Y[negative] = self.negative_sigmoid(X[negative])

        self.Y = Y
        return Y

    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        Sigmoid backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.Y is not None, "Forward pass not performed"
        return dY * self.Y * (1 - self.Y)

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0
    
# Test that Sigmoid has same results as sigmoid from old_layers.py that use numpy

def test_sigmoid():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    X = torch.randn(batch_size, input_dim, dtype=torch.float64)
    sigmoid = Sigmoid()
    Y = sigmoid.forward(X)
    sigmoid2 = ol.Sigmoid()
    Y2 = sigmoid2.forward(X.numpy())

    # Backward

    dY = torch.randn(batch_size, input_dim)
    dX = sigmoid.backward(dY)
    dX2 = sigmoid2.backward(dY.numpy())

    return np.allclose(Y.detach().numpy(), Y2) and np.allclose(dX.detach().numpy(), dX2)

class Tanh(Layer):
    def __init__(self):
        self.Y = None

    parameters = Layer.parameters
    gradients = Layer.gradients

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Tanh forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, input_dim) Output data
        """
        self.Y = torch.tanh(X)
        return self.Y

    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        Tanh backward pass
        Parameters:
            dY: (batch_size, input_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        assert self.Y is not None, "Forward pass not performed"
        return dY * (1 - self.Y * self.Y)

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0
    
# Test that Tanh has same results as tanh from old_layers.py that use numpy

def test_tanh():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    X = torch.randn(batch_size, input_dim, dtype=torch.float64)
    tanh = Tanh()
    Y = tanh.forward(X)
    tanh2 = ol.Tanh()
    Y2 = tanh2.forward(X.numpy())

    # Backward

    dY = torch.randn(batch_size, input_dim)
    dX = tanh.backward(dY)
    dX2 = tanh2.backward(dY.numpy())

    return np.allclose(Y.detach().numpy(), Y2) and np.allclose(dX.detach().numpy(), dX2)

def logsumexp(X: torch.Tensor, keepdim: bool=False) -> torch.Tensor:
    """
    Compute logsumexp
    Parameters:
        X: (batch_size, input_dim) Input data
    Returns:
        logsumexp: (batch_size, 1) logsumexp if keepdims is True, else (batch_size) logsumexp
    """
    max_X, _ = torch.max(X, dim=1, keepdim=True)
    if keepdim:
        return torch.log(torch.sum(torch.exp(X - max_X), dim=1, keepdim=True)) + max_X
    else:
        return torch.log(torch.sum(torch.exp(X - max_X), dim=1, keepdim=False)) + max_X.squeeze()

def logsoftmax(X: torch.Tensor) -> torch.Tensor:
    """
    Compute logsoftmax
    Parameters:
        X: (batch_size, input_dim) Input data
    Returns:
        logsoftmax: (batch_size, input_dim) logsoftmax
    """
    return X - logsumexp(X, keepdim=True)
   

# Test that logsumexp and logsoftmax have same results as logsumexp and logsoftmax from old_layers.py that use numpy

def test_logsumexp_logsoftmax():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    X = torch.randn(batch_size, input_dim, dtype=torch.float64)
    logsumexp_X = logsumexp(X, keepdim=True)
    logsumexp_X2 = ol.logsumexp(X.numpy(), keepdims=True)
    logsoftmax_X = logsoftmax(X)
    logsoftmax_X2 = ol.logsoftmax(X.numpy())

    return np.allclose(logsumexp_X.detach().numpy(), logsumexp_X2) and np.allclose(logsoftmax_X.detach().numpy(), logsoftmax_X2)

class CrossEntropyLoss():
    def __init__(self):
        pass

    def forward(self, Y_hat: torch.Tensor, Y: torch.Tensor) -> torch.Tensor:
        """
        Cross entropy loss forward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            loss: (torch.Tensor) Loss
        """
        individual_loss = torch.sum(- Y * torch.log(Y_hat), dim=1)
        return torch.mean(individual_loss)

    def backward(self, Y_hat: torch.Tensor, Y: torch.Tensor) -> torch.Tensor:
        """
        Cross entropy loss backward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            dY_hat: (batch_size, input_dim) Gradients of the next layer
        """
        return - Y / Y_hat

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0
    
# Test that CrossEntropyLoss has same results as cross_entropy_loss from old_layers.py that use numpy

def test_cross_entropy_loss():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    # Positive Y_hat and Y
    Y_hat = torch.abs(torch.randn(batch_size, input_dim))
    Y = torch.abs(torch.randn(batch_size, input_dim))

    cross_entropy_loss = CrossEntropyLoss()
    loss = cross_entropy_loss.forward(Y_hat, Y)
    cross_entropy_loss2 = ol.CrossEntropyLoss()
    loss2 = cross_entropy_loss2.forward(Y_hat.numpy(), Y.numpy())

    # Backward

    dY_hat = cross_entropy_loss.backward(Y_hat, Y)
    dY_hat2 = cross_entropy_loss2.backward(Y_hat.numpy(), Y.numpy())

    return np.allclose(loss.detach().numpy(), loss2) and np.allclose(dY_hat.detach().numpy(), dY_hat2)

class SoftmaxCrossEntropyLoss(Layer):
    def __init__(self):
        self.softmax = Softmax()
        self.cross_entropy_loss = CrossEntropyLoss()
        self.Y = None

    parameters = Layer.parameters
    gradients = Layer.gradients

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Softmax cross entropy loss forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y_hat: (batch_size, input_dim) Predicted values
        """
        self.Y_hat = self.softmax.forward(X)
        return self.Y_hat

    def loss(self, Y_hat: torch.Tensor, Y: torch.Tensor) -> torch.Tensor:
        """
        Softmax cross entropy loss forward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            loss: (torch.Tensor) Loss
        """
        return torch.mean(logsumexp(Y_hat) - torch.sum(Y * Y_hat, dim=1))

    def backward(self, Y: torch.Tensor) -> torch.Tensor:
        """
        Softmax cross entropy loss backward pass
        Parameters:
            Y: (batch_size, input_dim) Actual values
        Returns:
            dX: (batch_size, input_dim) Gradients of the current layer
        """
        assert self.Y_hat is not None, "Forward pass not performed"
        return self.Y_hat - Y

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0

# Test that SoftmaxCrossEntropyLoss has same results as softmax_cross_entropy_loss from old_layers.py that use numpy
def test_softmax_cross_entropy_loss():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    # Positive Y_hat and Y
    Y_hat = torch.abs(torch.randn(batch_size, input_dim, dtype=torch.float64))
    Y = torch.abs(torch.randn(batch_size, input_dim, dtype=torch.float64))

    softmax_cross_entropy_loss = SoftmaxCrossEntropyLoss()
    loss = softmax_cross_entropy_loss.loss(Y_hat, Y)
    softmax_cross_entropy_loss2 = ol.SoftmaxCrossEntropyLoss()
    loss2 = softmax_cross_entropy_loss2.loss(Y_hat.numpy(), Y.numpy())
    
    # Perform forward pass
    X = torch.randn(batch_size, input_dim, dtype=torch.float64)
    _ = softmax_cross_entropy_loss.forward(X)
    _ = softmax_cross_entropy_loss2.forward(X.numpy())

    # Backward
    dY_hat = softmax_cross_entropy_loss.backward(Y)
    dY_hat2 = softmax_cross_entropy_loss2.backward(Y.numpy())
    
    return np.allclose(loss.detach().numpy(), loss2) and np.allclose(dY_hat.detach().numpy(), dY_hat2)

class MSELoss():
    def __init__(self):
        pass

    def forward(self, Y_hat: torch.Tensor, Y: torch.Tensor) -> torch.Tensor:
        """
        MSE loss forward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            loss: (torch.Tensor) Loss
        """
        return torch.mean(torch.sum((Y_hat - Y) ** 2, dim=1))

    def backward(self, Y_hat: torch.Tensor, Y: torch.Tensor) -> torch.Tensor:
        """
        MSE loss backward pass
        Parameters:
            Y_hat: (batch_size, input_dim) Predicted values
            Y: (batch_size, input_dim) Actual values
        Returns:
            dY_hat: (batch_size, input_dim) Gradients of the next layer
        """
        return 2 * (Y_hat - Y)

    def parameter_count(self):
        """
        No parameters to update
        """
        return 0
    
# Test that MSELoss has same results as mse_loss from old_layers.py that use numpy
def test_mse_loss():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_dim = 5

    # Positive Y_hat and Y
    Y_hat = torch.abs(torch.randn(batch_size, input_dim))
    Y = torch.abs(torch.randn(batch_size, input_dim))

    mse_loss = MSELoss()
    loss = mse_loss.forward(Y_hat, Y)
    mse_loss2 = ol.MSELoss()
    loss2 = mse_loss2.forward(Y_hat.numpy(), Y.numpy())

    # Backward

    dY_hat = mse_loss.backward(Y_hat, Y)
    dY_hat2 = mse_loss2.backward(Y_hat.numpy(), Y.numpy())

    return np.allclose(loss.detach().numpy(), loss2) and np.allclose(dY_hat.detach().numpy(), dY_hat2)

class Sequential(Layer):
    def __init__(self, layers: List):
        self.layers = layers

    def __repr__(self):
        return "\n".join([f"{i+1}: {layer}" for i, layer in enumerate(self.layers)])

    def add(self, layer):
        """
        Add a layer to the sequential model
        """
        self.layers.append(layer)

    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Sequential forward pass
        Parameters:
            X: (batch_size, input_dim) Input data
        Returns:
            Y: (batch_size, output_dim) Output data
        """
        for layer in self.layers:
            X = layer.forward(X)
        return X

    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        """
        Sequential backward pass
        Parameters:
            dY: (batch_size, output_dim) Gradients of the next layer
        Returns:
            dX: (batch_size, input_dim) Gradients of this layer
        """
        for layer in reversed(self.layers):
            dY = layer.backward(dY)
        return dY

    def parameters(self) -> List[torch.Tensor]:
        """
        Return the parameters of the layer
        """
        return [param for layer in self.layers for param in layer.parameters()]
    
    def gradients(self) -> List[torch.Tensor]:
        """
        Return the gradients of the layer
        """
        return [grad for layer in self.layers for grad in layer.gradients()]

    def parameter_count(self):
        """
        Return the number of parameters in the layer
        """
        return sum([layer.parameter_count() for layer in self.layers])
    
# Image shape: (n_im, n_c, h, w)
#     n_im: Number of images
#     n_c: Number of channels
#     h: Height
#     w: Width

# Kernel shape: (n_k, n_c, h_k, w_k)
#     n_k: Number of kernels
#     n_c: Number of channels
#     h_k: Kernel height
#     w_k: Kernel width

def __correlate_2d_valid(a, b):
    a_h, a_w = a.shape
    b_h, b_w = b.shape
    
    o_h = a_h - b_h + 1
    o_w = a_w - b_w + 1
    
    out = torch.zeros((o_h, o_w))
    for i in range(o_h):
        for j in range(o_w):
            out[i, j] = torch.sum(a[i:i+b_h, j:j+b_w] * b)
            
    return out

def _correlate_2d_full(a, b):
    a_h, a_w = a.shape
    b_h, b_w = b.shape
    o_h = a_h + b_h - 1
    o_w = a_w + b_w - 1
    out = torch.zeros((o_h, o_w))
    for i in range(o_h):
        for j in range(o_w):
            a_h_start = max(0, i - b_h + 1)
            a_h_end = min(a_h, i + 1)
            b_h_start = max(0, b_h - i - 1)
            b_h_end = min(b_h, a_h + b_h - i - 1)
            a_w_start = max(0, j - b_w + 1)
            a_w_end = min(a_w, j + 1)
            b_w_start = max(0, b_w - j - 1)
            b_w_end = min(b_w, a_w + b_w - j - 1)
            out[i, j] = torch.sum(a[a_h_start:a_h_end, a_w_start:a_w_end] * b[b_h_start:b_h_end, b_w_start:b_w_end])
    return out

def _convolution_naive(X, W, b, padding=(0, 0), stride=(1, 1)):
    """
        X : (n_im, n_c, h, w) input
        W : (n_k, n_c, k_h, k_w) kernel weights
        b : (n_k, h_out, w_out) bias
    returns
        Z : (n_im, n_k, h_out, w_out) feature map
    """
    n_im, n_c, h, w = X.shape
    n_k, n_c, k_h, k_w = W.shape
    
    p_h, p_w = padding
    s_h, s_w = stride
    
    h_out = (h + 2 * p_h - k_h) // s_h + 1
    w_out = (w + 2 * p_w - k_w) // s_w + 1

    assert h_out % s_h == 0, "Invalid stride"
    assert w_out % s_w == 0, "Invalid stride"
    assert p_h < k_h and p_w < k_w, "Invalid padding, padding must be less than kernel size"
    
    Z = torch.zeros((n_im, n_k, h_out, w_out))
    for i in range(n_im):
        for j in range(n_k):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = max(0, k * s_h - p_h)
                    x_h_end = min(h, k * s_h + k_h - p_h)
                    x_w_start = max(0, l * s_w - p_w)
                    x_w_end = min(w, l * s_w + k_w - p_w)
                    
                    X_slice = X[i, :, x_h_start:x_h_end, x_w_start:x_w_end]

                    w_h_start = max(0, p_h - k * s_h)
                    w_h_end = min(k_h, h + p_h - k * s_h)
                    w_w_start = max(0, p_w - l * s_w)
                    w_w_end = min(k_w, w + p_w - l * s_w)
                    
                    W_slice = W[j, :, w_h_start:w_h_end, w_w_start:w_w_end]

                    Z[i, j, k, l] = torch.sum(X_slice * W_slice) + b[j]
                    
    return Z

def _conv_out_size(in_size, kernel_size, stride, padding):
    return (in_size - kernel_size + 2*padding) // stride + 1

def _convolution(X, W, b, stride=(1, 1), padding=(0, 0)):
    n_im, n_ch, h_in, w_in = X.shape
    n_k, _, h_k, w_k = W.shape
    h_out = _conv_out_size(h_in, h_k, stride[0], padding[0])
    w_out = _conv_out_size(w_in, w_k, stride[1], padding[1])
    X_unf = torch.nn.functional.unfold(X, (h_k, w_k), stride=stride, padding=padding)
    X_im2col = X_unf.permute(1, 0, 2).flatten(1)
    W_im2col = W.flatten(1)
    Z_flat = W_im2col @ X_im2col
    Z = Z_flat.reshape(n_k, n_im, h_out, w_out).permute(1, 0, 2, 3)
    return Z + b.reshape(1, n_k, 1, 1)

def _convolution_backward_naive(dZ, X, W, padding=(0, 0), stride=(1, 1)):
    """
        dZ : (n_im, n_k, h_out, w_out) gradients of feature map
        X : (n_im, n_c, h, w) input
        W : (n_k, n_c, k_h, k_w) kernel weights
    returns
        dX : (n_im, n_c, h, w) gradients of input
        dW : (n_k, n_c, k_h, k_w) gradients of kernel weights
        db : (n_k, h_out, w_out) gradients of bias
    """
    n_im, n_k, h_out, w_out = dZ.shape
    n_im, n_c, h, w = X.shape
    n_k, n_c, k_h, k_w = W.shape
    
    p_h, p_w = padding
    s_h, s_w = stride
    
    dX = torch.zeros((n_im, n_c, h, w))
    dW = torch.zeros((n_k, n_c, k_h, k_w))
    db = torch.zeros(n_k)
    
    for i in range(n_im):
        for j in range(n_k):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = max(0, k * s_h - p_h)
                    x_h_end = min(h, k * s_h + k_h - p_h)
                    x_w_start = max(0, l * s_w - p_w)
                    x_w_end = min(w, l * s_w + k_w - p_w)

                    X_slice = X[i, :, x_h_start:x_h_end, x_w_start:x_w_end]
                    dX_slice = dX[i, :, x_h_start:x_h_end, x_w_start:x_w_end]
                    
                    w_h_start = max(0, p_h - k * s_h)
                    w_h_end = min(k_h, h + p_h - k * s_h)
                    w_w_start = max(0, p_w - l * s_w)
                    w_w_end = min(k_w, w + p_w - l * s_w)

                    W_slice = W[j, :, w_h_start:w_h_end, w_w_start:w_w_end]
                    dW_slice = dW[j, :, w_h_start:w_h_end, w_w_start:w_w_end]
                    
                    dW_slice += X_slice * dZ[i, j, k, l]
                    dX_slice += W_slice * dZ[i, j, k, l]
                    
    for i in range(n_k):
        db[i] = torch.sum(dZ[:, i, :, :])
        
    return dX, dW, db

def _convolution_backward(dZ, X, W, stride=(1, 1), padding=(0, 0)):
    n_im, n_ch, h_in, w_in = X.shape
    n_k, _, h_k, w_k = W.shape
    h_out = _conv_out_size(h_in, h_k, stride[0], padding[0])
    w_out = _conv_out_size(w_in, w_k, stride[1], padding[1])
    dZ_unf = dZ.permute(1, 0, 2, 3).flatten(1)
    W_im2col = W.flatten(1)
    dX_im2col = W_im2col.T @ dZ_unf
    dX_im2col_reshape = dX_im2col.reshape(n_ch, h_k, w_k, n_im, h_out, w_out).permute(3, 0, 1, 2, 4, 5)
    # n_im, n_c, h_k, w_k, h_out, w_out
    dX = torch.nn.functional.fold(dX_im2col_reshape.flatten(1,3).flatten(2), (h_in, w_in), (h_k, w_k), padding=padding, stride=stride)
    X_im2col = torch.nn.functional.unfold(X, (h_k, w_k), stride=stride, padding=padding).permute(1, 0, 2).flatten(1)
    dW_im2col = dZ_unf @ X_im2col.T
    dW = dW_im2col.reshape(n_k, n_ch, h_k, w_k)
    db = dZ.sum(dim=(0, 2, 3))
    return dX, dW, db

# Test that convolution has same results as convolution from old_layers.py that use numpy

def test_convolution():
    import old_layers as ol
    import numpy as np

    batch_size = 10
    input_h_w = 5
    input_c = 3
    kernel_h_w = 3
    kernel_c = 3
    n_kernels = 5
    padding = (0, 0)
    stride = (1, 1)
    out_h_w = _conv_out_size(input_h_w, kernel_h_w, stride[0], padding[0])

    X = torch.randn(batch_size, input_c, input_h_w, input_h_w, dtype=torch.float64)
    W = torch.randn(n_kernels, kernel_c, kernel_h_w, kernel_h_w, dtype=torch.float64)
    b = torch.randn(n_kernels, dtype=torch.float64)

    Z = _convolution(X, W, b, padding=padding, stride=stride)
    Z2 = ol._convolution_naive(X.numpy(), W.numpy(), b.numpy(), padding=padding, stride=stride)

    # Backward

    dZ = torch.randn(batch_size, n_kernels, out_h_w, out_h_w, dtype=torch.float64)
    dX, dW, db = _convolution_backward(dZ, X, W, padding=padding, stride=stride)
    dX2, dW2, db2 = ol._convolution_backward_naive(dZ.numpy(), X.numpy(), W.numpy(), padding=padding, stride=stride)
    
    precision = 1e-3

    return (np.allclose(Z.detach().numpy(), Z2, atol=precision)
        and np.allclose(dX.detach().numpy(), dX2, atol=precision) 
        and np.allclose(dW.detach().numpy(), dW2, atol=precision) 
        and np.allclose(db.detach().numpy(), db2, atol=precision))
    
def _average_pooling_naive(X, kernel_size=(2, 2), stride=(2, 2)):
    """
        X : (n_im, n_c, h, w) input
    returns
        Z : (n_im, n_c, h_out, w_out) feature map
    """
    n_im, n_c, h, w = X.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    h_out = h // s_h
    w_out = w // s_w
    
    assert h_out * s_h == h, "Invalid stride"
    assert w_out * s_w == w, "Invalid stride"
    
    Z = torch.zeros((n_im, n_c, h_out, w_out))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    Z[i, j, k, l] = torch.mean(X[i, j, x_h_start:x_h_end, x_w_start:x_w_end])
    return Z

def _average_pooling(X, kernel_size=(2, 2), stride=(2, 2)):
    n_im, n_ch, h_in, w_in = X.shape
    h_out = _conv_out_size(h_in, kernel_size[0], stride[0], 0)
    w_out = _conv_out_size(w_in, kernel_size[1], stride[1], 0)
    return torch.nn.functional.unfold(X, kernel_size, stride=stride).reshape(n_im, n_ch, kernel_size[0] * kernel_size[1], h_out, w_out).mean(dim=2)

def _max_pooling_naive(X, kernel_size=(2, 2), stride=(2, 2)):
    """
        X : (n_im, n_c, h, w) input
    returns
        Z : (n_im, n_c, h_out, w_out) feature map
    """
    n_im, n_c, h, w = X.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    h_out = h // s_h
    w_out = w // s_w
    
    assert h_out * s_h == h, "Invalid stride"
    assert w_out * s_w == w, "Invalid stride"
    
    Z = torch.zeros((n_im, n_c, h_out, w_out))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    Z[i, j, k, l] = torch.max(X[i, j, x_h_start:x_h_end, x_w_start:x_w_end])
    return Z

def _max_pooling(X, kernel_size=(2, 2), stride=(2, 2)):
    n_im, n_ch, h_in, w_in = X.shape
    h_out = _conv_out_size(h_in, kernel_size[0], stride[0], 0)
    w_out = _conv_out_size(w_in, kernel_size[1], stride[1], 0)
    return torch.nn.functional.unfold(X, kernel_size, stride=stride).reshape(n_im, n_ch, kernel_size[0] * kernel_size[1], h_out, w_out).max(dim=2).values

def _average_pooling_backward_naive(dZ, X_shape, kernel_size=(2, 2), stride=(2, 2)):
    """
        dZ : (n_im, n_c, h_out, w_out) gradients of feature map
        X : (n_im, n_c, h, w) input
    returns
        dX : (n_im, n_c, h, w) gradients of input
    """
    n_im, n_c, h, w = X_shape
    n_im, n_c, h_out, w_out = dZ.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    dX = torch.zeros((n_im, n_c, h, w))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    dX[i, j, x_h_start:x_h_end, x_w_start:x_w_end] += dZ[i, j, k, l] / (k_h * k_w)
    return dX

def _average_pooling_backward(dZ, X_shape, kernel_size=(2, 2), stride=(2, 2)):
    dX_im2col = dZ.flatten(2).unsqueeze(2).repeat(1, 1, kernel_size[0]*kernel_size[1], 1).flatten(1, 2) / (kernel_size[0]*kernel_size[1])
    dX = torch.nn.functional.fold(dX_im2col, X_shape[2:], kernel_size, stride=stride)
    return dX

def _max_pooling_backward_naive(dZ, X, cached_Z, kernel_size=(2, 2), stride=(2, 2)):
    """
        dZ : (n_im, n_c, h_out, w_out) gradients of feature map
        X : (n_im, n_c, h, w) input
    returns
        dX : (n_im, n_c, h, w) gradients of input
    """
    n_im, n_c, h, w = X.shape
    n_im, n_c, h_out, w_out = dZ.shape
    k_h, k_w = kernel_size
    s_h, s_w = stride
    
    dX = torch.zeros((n_im, n_c, h, w))
    for i in range(n_im):
        for j in range(n_c):
            for k in range(h_out):
                for l in range(w_out):
                    x_h_start = k * s_h
                    x_h_end = k * s_h + k_h
                    x_w_start = l * s_w
                    x_w_end = l * s_w + k_w
                    
                    X_slice = X[i, j, x_h_start:x_h_end, x_w_start:x_w_end]
                    dX_slice = dX[i, j, x_h_start:x_h_end, x_w_start:x_w_end]
                    
                    Z_slice = cached_Z[i, j, k, l]
                    
                    dX_slice += (X_slice == Z_slice) * dZ[i, j, k, l]
    return dX

def _max_pooling_backward(dZ, X, cached_Z, kernel_size=(2, 2), stride=(2, 2)):
    X_im2col = torch.nn.functional.unfold(X, kernel_size, stride=stride)
    dZ_im2col = dZ.flatten(2).unsqueeze(2).repeat(1, 1, kernel_size[0]*kernel_size[1], 1).flatten(1,2)
    cached_Z_im2col = cached_Z.flatten(2).unsqueeze(2).repeat(1, 1, kernel_size[0]*kernel_size[1], 1).flatten(1,2)
    dX_im2col = dZ_im2col.where(X_im2col == cached_Z_im2col, 0)
    dX = torch.nn.functional.fold(dX_im2col, X.shape[2:], kernel_size, stride=stride)
    return dX

class AveragePooling(Layer):
    def __init__(self, kernel_size: int, stride: int):
        self.kernel_size = (kernel_size, kernel_size)
        self.stride = (stride, stride)
        self.X_shape = None

    parameters = Layer.parameters
    gradients = Layer.gradients
    
    def __repr__(self):
        return f"MeanPooling(kernel_size={self.kernel_size}, stride={self.stride})"
    
    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_channels, h, w) Input data
        Returns:
            Y: (batch_size, output_channels, h_out, w_out) Output data
        """
        self.X_shape = X.shape
        return _average_pooling(X, kernel_size=self.kernel_size, stride=self.stride)
    
    def backward(self, dZ: torch.Tensor) -> torch.Tensor:
        """
        Perform a Backward pass
        Parameters:
            dZ: (batch_size, output_channels, h_out, w_out) Gradients of the next layer
        Returns:
            dX: (batch_size, input_channels, h, w) Gradients of this layer
        """
        assert self.X_shape is not None, "Forward pass not performed"
        return _average_pooling_backward(dZ, self.X_shape, kernel_size=self.kernel_size, stride=self.stride)
    
    def parameter_count(self):
        return 0
    
class MaxPooling(Layer):
    def __init__(self, kernel_size: int, stride: int):
        self.kernel_size = (kernel_size, kernel_size)
        self.stride = (stride, stride)
        self.cached_X = None
        self.cached_Z = None

    parameters = Layer.parameters
    gradients = Layer.gradients
    
    def __repr__(self):
        return f"MaxPooling(kernel_size={self.kernel_size}, stride={self.stride})"
    
    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_channels, h, w) Input data
        Returns:
            Y: (batch_size, output_channels, h_out, w_out) Output data
        """
        self.cached_X = X
        self.cached_Z = _max_pooling(X, kernel_size=self.kernel_size, stride=self.stride)
        return self.cached_Z
    
    def backward(self, dZ: torch.Tensor) -> torch.Tensor:
        """
        Perform a Backward pass
        Parameters:
            dZ: (batch_size, output_channels, h_out, w_out) Gradients of the next layer
        Returns:
            dX: (batch_size, input_channels, h, w) Gradients of this layer
        """
        assert self.cached_X is not None, "Forward pass not performed"
        assert self.cached_Z is not None, "Forward pass not performed"
        return _max_pooling_backward(dZ, self.cached_X, self.cached_Z, kernel_size=self.kernel_size, stride=self.stride)
    
    def parameter_count(self):
        return 0

class Convolution(Layer):
    def __init__(self, input_channels: int, output_channels: int, kernel_size: int,
                 padding: int=0, stride: int=1, weight_initialisation: weight_initialisation_t = "xavier"):
        self.input_channels = input_channels
        self.output_channels = output_channels
        self.kernel_size = kernel_size
        self.padding = (padding, padding)
        self.stride = (stride, stride)

        self.initialise_weights(weight_initialisation)
        self.X = None
        
    def __repr__(self):
        return f"Convolution(input_channels={self.input_channels}, output_channels={self.output_channels}, kernel_size={self.kernel_size}, padding={self.padding}, stride={self.stride})"

    def initialise_weights(self, method: weight_initialisation_t = "xavier"):
        """
        Initialise the weights
        Parameters:
            method: "normal", "uniform", "xavier" or "he"
        """
        if method == "normal":
            self.W = torch.randn(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size)
            self.b = torch.zeros(self.output_channels)
        elif method == "uniform":
            self.W = torch.rand(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size)
            self.b = torch.zeros(self.output_channels)
        elif method == "xavier":
            self.W = torch.randn(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size) / torch.sqrt(torch.tensor(self.input_channels))
            self.b = torch.zeros(self.output_channels)
        elif method == "he":
            self.W = torch.randn(self.output_channels, self.input_channels, self.kernel_size, self.kernel_size) / torch.sqrt(torch.tensor(self.input_channels / 2))
            self.b = torch.zeros(self.output_channels)
        else:
            raise ValueError("Invalid weight initialisation method")
        self.dW = torch.zeros_like(self.W)
        self.db = torch.zeros_like(self.b)
        
    def forward(self, X: torch.Tensor) -> torch.Tensor:
        """
        Perform a Forward pass
        Parameters:
            X: (batch_size, input_channels, h, w) Input data
        Returns:
            Y: (batch_size, output_channels, h_out, w_out) Output data
        """
        assert X.shape[1] == self.input_channels, f"Input dimension mismatch. Expected {self.input_channels}, got {X.shape[1]}"
        self.X = X
        return _convolution(X, self.W, self.b, padding=self.padding, stride=self.stride)

    def backward(self, dZ: torch.Tensor) -> torch.Tensor:
        """
        Perform a Backward pass
        Parameters:
            dZ: (batch_size, output_channels, h_out, w_out) Gradients of the next layer
        Returns:
            dX: (batch_size, input_channels, h, w) Gradients of this layer
        """
        assert self.X is not None, "Forward pass not performed"
        assert dZ.shape[1] == self.output_channels, f"Output dimension mismatch. Expected {self.output_channels}, got {dZ.shape[1]}"
        dX, dW, db = _convolution_backward(dZ, self.X, self.W, padding=self.padding, stride=self.stride)
        self.dW[:] = dW
        self.db[:] = db
        return dX
    
    def parameters(self) -> List[Tensor]:
        return [self.W, self.b]
    
    def gradients(self) -> List[Tensor]:
        return [self.dW, self.db]
    
    def output_shape(self, input_shape: Tuple[int, int, int, int]) -> Tuple[int, int, int, int]:
        """
        Return the output shape given an input shape
        """
        # TODO verify the correctness of this
        n_im, n_c, h, w = input_shape
        h_out = (h + 2 * self.padding[0] - self.kernel_size) // self.stride[0] + 1
        w_out = (w + 2 * self.padding[1] - self.kernel_size) // self.stride[1] + 1
        return (n_im, self.output_channels, h_out, w_out)
    
    def parameter_count(self):
        return self.output_channels * self.input_channels * self.kernel_size * self.kernel_size + self.output_channels

class Reshape(Layer):
    def __init__(self, new_shape: Tuple):
        self.new_shape = new_shape
        self.old_shape = None

    parameters = Layer.parameters
    gradients = Layer.gradients

    def __repr__(self):
        if self.old_shape:
            return f"Reshape(old_shape={self.old_shape}, new_shape={self.new_shape})"
        else:
            return f"Reshape(new_shape={self.new_shape})"
    
    def forward(self, X: torch.Tensor) -> torch.Tensor:
        self.old_shape = X.shape[1:]
        batch_size = X.shape[0]
        return torch.reshape(X, (batch_size,) + self.new_shape)
    
    def backward(self, dY: torch.Tensor) -> torch.Tensor:
        assert self.old_shape is not None, "Forward pass not performed"
        batch_size = dY.shape[0]
        return torch.reshape(dY, (batch_size,) + self.old_shape)
    
    def reverse(self):
        assert self.old_shape is not None, "Forward pass not performed"
        inv = Reshape(self.old_shape)
        inv.old_shape = self.new_shape
        return inv

    def parameter_count(self):
        """
        No parameters
        """
        return 0